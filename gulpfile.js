var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('autoprefixer'),
	concat = require('gulp-concat'),
	browserSync = require('browser-sync'),
	reload      = browserSync.reload;
var cssnano = require('cssnano');
var postcss = require('gulp-postcss');
var gutil = require('gulp-util');
var connect = require('gulp-connect-php');


var order = require('gulp-order');
var print = require('gulp-print');
var log = gutil.log;
var c = gutil.colors;


var siteroot = 'htdocs';

// CONFIG
var processors = [
        autoprefixer({browsers: ['last 3 versions']}),
    ];
var miniprocessors = [
		autoprefixer({browsers: ['last 3 versions']}),
        cssnano
    ];


var scriptorder = [
	    'vue.js',
	    'jquery.js',
	    'enquire.js',
	    'sidenotes.js',
	    'velocity.js',
	    'vue-resource.js',
	    'vue-router.js',
	    'main.js',
	  ];





// funkce co se pousti kdyz je eror v kompilaci gutil.beep() je zvuk, kdyztak vykomentovat
var onError = function (err) {  
  gutil.beep();
  console.log(err);
};


gulp.task('browser-sync',['connect'], function() {
    browserSync.init({
      proxy: '127.0.0.1:8020',
      port: 8080,
      open: true,
      browser: 'google chrome canary',
      notify: false
    }, function (err, bs) {
    	 if (err) { console.log(err); }
		    if (!err) {
		        console.log('BrowserSync is ready!');
		    }
	});
});


gulp.task('connect', function(cb) {
    connect.server({ base: siteroot, port: 8020}, 
    function() {
    	console.log('PHP server initialized, starting BrowserSync');
    	cb();
    });
});



// sass
gulp.task('sass', function() {
		gulp.src('sass/main.scss')
		.pipe(sass({
		      outputStyle: 'expanded',
		      onSuccess: function(css) {
		        var dest = css.stats.entry.split('/');
		        log(c.green('sass'), 'compiled to', dest[dest.length - 1]);
		      },
		      onError: function(err, res) {
		        log(c.red('Sass failed to compile'));
		        log(c.red('> ') + err.file.split('/')[err.file.split('/').length - 1] + ' ' + c.underline('line ' + err.line) + ': ' + err.message);
		      }
		    }).on('error', sass.logError))
		.pipe(postcss(processors))
		.pipe(gulp.dest(siteroot + '/assets/css'))
		.pipe(reload({stream:true}));
	});




gulp.task('minijs', function() {
	return  gulp.src('js/*.js')
	.pipe(order(scriptorder))
	.pipe(order(scriptorder, {base : 'js' }))
	.pipe(concat('main.js')) // spojeni vseho do main.js
    .pipe(uglify())
	.pipe(gulp.dest(siteroot + '/assets/js'));
});
 
gulp.task('script', function() {
	return  gulp.src('js/*.js')
	.pipe(order(scriptorder, {base : 'js' }))
	.pipe(print())
   	.pipe(concat('main.js')) // spojeni vseho do main.js
	.pipe(gulp.dest(siteroot + '/assets/js'))        
	.pipe(reload({stream:true}));;
});

gulp.task('bs-reload', function() {
	reload();
});

gulp.task('default', ['browser-sync','sass', 'script'], function () {
	gulp.watch(['js/*.js'], ['script']);
	gulp.watch(['sass/*.scss'], ['sass']);
	gulp.watch([siteroot + '/site/templates/*.php', siteroot + '/site/snippets/*.php'], ['bs-reload']);
});
